<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー新規登録</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br />
                <label for="login_id">ログインID</label> <input name="login_id" id="login_id" /> <br />
                <label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />
                <label for="name">氏名</label> <input name="name" id="name" /><br />
                <br />
                <SELECT NAME="branches" >
                	<OPTION SELECTED>所属支店は？</OPTION>
                	<c:forEach items="${branches}" var="branch">
                		<option value="${branch.id}">${branch.name}</option>
                	</c:forEach>
               </SELECT> <br />
                <br />
                <SELECT NAME="departments_positions"><OPTION SELECTED>部署・役職は？
                                   <OPTION VALUE="over90">９０歳以上
                                   <OPTION VALUE="80-89">８０歳～８９歳
                                   <OPTION VALUE="10-19">１０歳～１９歳
                                   <OPTION VALUE="under10">１０歳未満
                </SELECT> <br />
                <br /> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
            <div class="copyright">Copyright(c)Takeuchi Mizuki</div>
        </div>
    </body>
</html>