<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
            <div class="header">
                <c:if test="${ empty loginUser }">
                    <a href="login">ログイン</a>
                    <a href="signup">新規登録する</a>
                </c:if>
                <c:if test="${ not empty loginUser }">
                    <a href="./">ホーム</a>
                    <a href="settings">設定</a>
                    <a href="logout">ログアウト</a>
                </c:if>
            </div>
                <c:if test="${ not empty loginUser }">
                    <div class="profile">
                        <div class="name"><h2>
                            <c:out value="${loginUser.name}" /></h2>
                        </div>
                        <div class="account">
                            @<c:out value="${loginUser.loginId}" />
                        </div>
                    </div>
                </c:if>
            <div class="form-area">
                <c:if test="${ isShowPostForm }">
                    <form action="newPost" method="post">
                        カテゴリー(10文字まで)<br />
                        <textarea name="category" cols="10" rows="1" class="post-box"></textarea>
                        <br />
                        件名(30文字まで)<br />
                        <textarea name="subject" cols="30" rows="1" class="post-box"></textarea>
                        <br />
                        本文(1000文字まで)<br />
                        <textarea name="text" cols="100" rows="10" class="post-box"></textarea>
                        <br />
                        <input type="submit" value="投稿">
                    </form>
                </c:if>
            </div>
            <div class="posts">
                <c:forEach items="${posts}" var="post">
                    <div class="post">
                        <div class="account">
                            <span class="name"><c:out value="${loginUser.name}" /></span>
                            <span class="login_id">@<c:out value="${loginUser.loginId}" /></span>
                        </div>
                        <div class="category"><c:out value="${post.category}" /></div>
                        <div class="subject"><c:out value="${post.subject}" /></div>
                        <div class="text"><c:out value="${post.text}" /></div>
                        <div class="date"><fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
                    </div>
                </c:forEach>
            </div>
            <div class="copyright">Copyright(c)Takeuchi Mizuki</div>
        </div>
    </body>
</html>