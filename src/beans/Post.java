package beans;

import java.io.Serializable;
import java.util.Date;


public class Post implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer userId;
    private String subject;
    private String text;
    private String category;
	private Date createdDate;
    private Date updatedDate;

    public Integer getId() {
		return id;
	}
    public void setId(Integer id) {
		this.id = id;
	}
    public Integer getUserId() {
		return userId;
	}
    public void setUserId(Integer userId) {
		this.userId = userId;
	}
    public String getSubject() {
		return subject;
	}
    public void setSubject(String subject) {
		this.subject = subject;
	}
    public String getText() {
		return text;
	}
    public void setText(String text) {
		this.text = text;
	}
    public String getCategory() {
		return category;
	}
    public void setCategory(String category) {
		this.category = category;
	}
    public Date getCreatedDate() {
		return createdDate;
	}
    public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
