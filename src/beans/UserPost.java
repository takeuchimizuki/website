package beans;

import java.io.Serializable;
import java.util.Date;

public class UserPost implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private Integer userId;
    private String login_id;
    private String category;
    private String subject;
    private String text;
    private Date created_date;

    public Integer getId() {
		return id;
	}
    public void setId(Integer id) {
		this.id = id;
	}
    public String getName() {
		return name;
	}
    public void setName(String name) {
		this.name = name;
	}
    public Integer getUserId() {
		return userId;
	}
    public void setUserId(Integer userId) {
		this.userId = userId;
	}
    public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getCategory() {
		return category;
	}
    public void setCategory(String category) {
		this.category = category;
	}
    public String getSubject() {
		return subject;
	}
    public void setSubject(String subject) {
		this.subject = subject;
	}
    public String getText() {
		return text;
	}
    public void setText(String text) {
		this.text = text;
	}
    public Date getCreated_date() {
		return created_date;
	}
    public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
}
