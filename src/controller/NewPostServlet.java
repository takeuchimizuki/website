package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.PostService;

@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Post post = new Post();
            post.setCategory(request.getParameter("category"));
            post.setSubject(request.getParameter("subject"));
            post.setText(request.getParameter("text"));
            post.setUserId(user.getId());

            new PostService().register(post);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String category = request.getParameter("category");
        String subject = request.getParameter("subject");
        String text = request.getParameter("text");

        if (StringUtils.isEmpty(category) == true) {
        	messages.add("カテゴリーを入力してください");
        }
        if (StringUtils.isEmpty(subject) == true) {
        	messages.add("件名を入力してください");
        }
        if (StringUtils.isEmpty(text) == true) {
        	messages.add("本文を入力してください");
        }

        if (10 < category.length()) {
        	messages.add("10文字以下で入力してください");
        }
        if (30 < subject.length()) {
        	messages.add("30文字以下で入力してください");
        }
        if (1000 < text.length()) {
        	messages.add("1000文字以下で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}