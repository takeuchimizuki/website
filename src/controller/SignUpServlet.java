package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.DepartmentPosition;
import beans.User;
import service.DBService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        DBService dBService = new DBService();

        List<Branch> branches = dBService.getBranches();
        request.setAttribute("branches", branches);


        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLoginId(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));

            new DBService().register(user);

            Branch branch = new Branch();
            branch.setId(Integer.parseInt(request.getParameter("id")));
            branch.setName(request.getParameter("name"));

            new DBService().register(branch);

            DepartmentPosition departmentPosition = new DepartmentPosition();
            departmentPosition.setId(Integer.parseInt(request.getParameter("id")));
            departmentPosition.setName(request.getParameter("name"));

            new DBService().register(departmentPosition);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String loginId = request.getParameter("login_id");
        String password = request.getParameter("password");
        String name = request.getParameter("name");

        if (StringUtils.isEmpty(loginId) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("氏名を入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}