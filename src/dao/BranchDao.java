package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

    public void insert(Connection connection, Branch branch) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("id");
            sql.append(", name");
            sql.append(") VALUES (");
            sql.append("?"); // id
            sql.append(", ?"); // name
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, branch.getId());
            ps.setString(2, branch.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public Branch getBranch(Connection connection, Integer branchId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM branchs WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, branchId);

            ResultSet rs = ps.executeQuery();
            List<Branch> branchList = toBranchList(rs);
            if (branchList.isEmpty() == true) {
                return null;
            } else if (2 <= branchList.size()) {
                throw new IllegalStateException("2 <= branchList.size()");
            } else {
                return branchList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Branch> getBranches(Connection conn) {
    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM branches";
    		ps = conn.prepareStatement(sql);

    		ResultSet rs = ps.executeQuery();
    		return toBranchList(rs);

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }

    }
    private List<Branch> toBranchList(ResultSet rs) throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                Integer id = rs.getInt("id");
                String name = rs.getString("name");

                Branch branch = new Branch();
                branch.setId(id);
                branch.setName(name);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}