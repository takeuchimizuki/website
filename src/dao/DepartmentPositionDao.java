package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.DepartmentPosition;
import exception.SQLRuntimeException;

public class DepartmentPositionDao {

    public void insert(Connection connection, DepartmentPosition departmentPosition) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("id");
            sql.append(", name");
            sql.append(") VALUES (");
            sql.append("?"); // id
            sql.append(", ?"); // name
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, departmentPosition.getId());
            ps.setString(2, departmentPosition.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public DepartmentPosition getDepartmentPosition(Connection connection, Integer departmentPositionId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM departments_positions WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, departmentPositionId);

            ResultSet rs = ps.executeQuery();
            List<DepartmentPosition> departmentPositionList = toDepartmentPositionList(rs);
            if (departmentPositionList.isEmpty() == true) {
                return null;
            } else if (2 <= departmentPositionList.size()) {
                throw new IllegalStateException("2 <= departmentPositionList.size()");
            } else {
                return departmentPositionList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<DepartmentPosition> toDepartmentPositionList(ResultSet rs) throws SQLException {

        List<DepartmentPosition> ret = new ArrayList<DepartmentPosition>();
        try {
            while (rs.next()) {
                Integer id = rs.getInt("id");
                String name = rs.getString("name");

                DepartmentPosition departmentPosition = new DepartmentPosition();
                departmentPosition.setId(id);
                departmentPosition.setName(name);

                ret.add(departmentPosition );
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
