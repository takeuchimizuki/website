package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import beans.DepartmentPosition;
import beans.User;
import dao.BranchDao;
import dao.DepartmentPositionDao;
import dao.UserDao;
import utils.CipherUtil;

public class DBService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<Branch> getBranches() {
    	Connection conn = null;
    	try {
    		conn = getConnection();
    		BranchDao dao = new BranchDao();
    		return dao.getBranches(conn);
        } catch (RuntimeException e) {
            rollback(conn);
            throw e;
        } catch (Error e) {
            rollback(conn);
            throw e;
        } finally {
            close(conn);
        }

    }
	public void register(Branch branch) {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            branchDao.insert(connection, branch);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public void register(DepartmentPosition departmentPosition) {

        Connection connection = null;
        try {
            connection = getConnection();

            DepartmentPositionDao departmentPositionDao = new DepartmentPositionDao();
            departmentPositionDao.insert(connection, departmentPosition);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User getUser(Integer userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.getUser(connection, userId);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public Branch getBranch(Integer branchId) {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            Branch branch = branchDao.getBranch(connection, branchId);

            commit(connection);

            return branch;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public DepartmentPosition getDepartmentPosition(Integer departmentPositionId) {

        Connection connection = null;
        try {
            connection = getConnection();

            DepartmentPositionDao departmentPositionDao = new DepartmentPositionDao();
            DepartmentPosition departmentPosition = departmentPositionDao.getDepartmentPosition(connection, departmentPositionId);

            commit(connection);

            return departmentPosition;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}